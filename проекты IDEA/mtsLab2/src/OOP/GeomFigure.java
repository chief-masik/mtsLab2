package OOP;

public abstract class GeomFigure {
    private String color;

    public GeomFigure(String color) {
        this.color = color;
    }

    public String getColor() {       // гетер
        return color;
    }

    public abstract double getPerimeter();

    public abstract double getArea();

    public abstract String toString();

}